package main

import (
	"app/go_pkg/login"
	"app/go_pkg/public"
	"app/go_pkg/records"
	"app/go_pkg/register"
	"fmt"
	"html/template"
	"net/http"
)

var templ *template.Template

func init() {
	templ = template.Must(template.ParseFiles("dist/index.html"))
}

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		templ.ExecuteTemplate(w, "index.html", nil)
	})

	mux.Handle("/api/register", &register.Handle{})
	mux.Handle("/api/login", &login.Handle{})
	mux.Handle("/api/public/occurence_master", &public.OccurenceMaster{})
	mux.Handle("/api/data/register", &records.Add{})

	mux.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("dist/css"))))
	mux.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("dist/js"))))
	mux.Handle("/img/", http.StripPrefix("/img/", http.FileServer(http.Dir("dist/img"))))

	fmt.Println("Server is hosting at port 8081...")
	http.ListenAndServe(":8081", mux)
}
