import Vue from "vue";
import Router from "vue-router";
import store from './store'
// import Home from "./views/Home.vue";

Vue.use(Router);

const router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            name: "summary",
            component: () => import("./views/Summary.vue"),
            // meta: {
            //     requiresPreloaded: true,
            //     requiresAuth: true
            // }
        },
        {
            path: "/overall",
            name: "overall",
            component: () => import("./views/Overall.vue"),
            // meta: {
            //     requiresPreloaded: true,
            //     requiresAuth: true
            // }
        },
        {
            path: "/login",
            name: "login",
            component: () => import("./views/Login.vue"),
            // meta: {
            //     requiresPreloaded: true,
            //     notLoggedIn: true
            // }
        },
        {
            path: "/register",
            name: "register",
            component: () => import("./views/Register.vue"),
            // meta: {
            //     requiresPreloaded: true,
            //     notLoggedIn: true
            // }
        },
        {
            path: "*",
            name: "pageNotFound",
            component: () => import("./views/PageNotFound.vue")
        }
    ]
});

router.beforeEach((to, from, next) => {
    let requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    let notLoggedIn  = to.matched.some(record => record.meta.notLoggedIn);
    let isLoggedIn   = store.state.isLoggedIn;

    if (store.state.refreshedPage == "") {
        store.state.refreshedPage = to.name;
    }

    if (requiresAuth && !isLoggedIn) {
        next({
            name: 'login'
        })
    }
    
    if (notLoggedIn && isLoggedIn) {
        next({
            name: 'summary'
        })
    }

    next()
});

export default router;
