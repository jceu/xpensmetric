import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import $ from "jquery";

import './assets/css/global.scss';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

Vue.mixin({
    methods: {
        saveToken(token){
            localStorage.setItem("xpens_ttk", token);
        },
        getToken(){
            return localStorage.getItem("xpens_ttk") || "";
        },
        removeToken(){
            localStorage.removeItem("xpens_ttk");
        },
        addComma(val){
            if(typeof val == "number"){
                val = val.toString();
            }
            return val.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        rmvComma(str) {
            return (str.replace(/,/g,''));
        },
        checkLogin(){
            return new Promise(resolve => {
                this.$store.state.preloaderMSG = "Checking authentication";

                $.ajax(store.state.hostServer + "api/login", {
                    method:   "POST",
                    dataType: "JSON",
                    data: {
                        token: this.getToken()
                    }
                })
                .done((response) => {
                    if(response.Status){
                        this.$store.state.isLoggedIn = true;
                    }
                    
                    resolve(response);
                })
                .fail((xhr, status, error) => {
                    console.log(status);
                    console.log(error);
                });
            });
        }
    }
});

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount("#app");
