import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        hostServer: "http://localhost:8081/",
        // hostServer: "/",
        isLoggedIn: false,
        isPreloaded: false,
        preloaderMSG: ""
    },
    mutations: {},
    actions: {},
});
