package cors

import (
	"net/http"
)

func EnableAll(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
