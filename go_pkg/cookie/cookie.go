package cookie

import (
	"net/http"

	"github.com/gorilla/securecookie"
)

const (
	COOKIE_HASH_KEY = "g2zg7SrJWi9zm2ghqViM4N0ZqOauF5O6"
	BLOCK_KEY       = "47G2OQPeNvryqqbJiJEPEAQzp32A8S6X"
)

var cooky *securecookie.SecureCookie = securecookie.New([]byte(COOKIE_HASH_KEY), []byte(BLOCK_KEY))

func WriteCookie(w *http.ResponseWriter, cookieName string, value string) {
	http.SetCookie(*w, &http.Cookie{
		Name:   cookieName,
		Value:  Encode(cookieName, value),
		MaxAge: 60 * 60 * 24 * 7,
		Path:   "/",
	})
}

func ReadCookie(r *http.Request, cookieName string) string {
	cookie, err := r.Cookie(cookieName)
	if err != nil {
		return ""

	}

	return Decode(cookieName, cookie.Value)
}

func Encode(name, val string) string {
	value, _ := cooky.Encode(name, val)
	return value
}

func Decode(name, hash string) string {
	var value string
	err := cooky.Decode(name, hash, &value)
	if err != nil {
		return ""
	}

	return value
}
