package login

import (
	"app/go_pkg/cookie"
	"app/go_pkg/cors"
	"app/go_pkg/db_users"
	"encoding/json"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

type Handle struct {
}

type response struct {
	Status  bool
	Message string
	Token   string
}

func (h *Handle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	cors.EnableAll(&w)

	if r.Method != http.MethodPost {
		writeResponse(&w, false, "GET method is not allowed", "")
		return
	}

	err := r.ParseForm()
	if err != nil {
		writeResponse(&w, false, "Posted form cannot be parse", "")
		return
	}

	// Get Cookie
	// authCookie := cookie.ReadCookie(r, "AUTH")
	authCookie := r.Form.Get("token")

	// Check Session
	if db_users.IsValidSessionKey(cookie.Decode("AUTH", authCookie)) {
		writeResponse(&w, true, "", authCookie)
		return
	}

	username := r.Form.Get("username")
	password := r.Form.Get("password")

	// GET DATA BY USERNAME
	user, err := db_users.GetByUsername(username)
	if err != nil {
		writeResponse(&w, false, "Username or Password is incorrect", "")
		return
	}

	// VERIFY PASSWORD
	err = db_users.VerifyPassword(password, user.Password)
	if err != nil {
		writeResponse(&w, false, "Username or Password is incorrect", "")
		return
	}

	// Check session key from DB
	sessionKeyString := db_users.GetSessionKey(username)

	// If session key from DB is valid, use it
	if sessionKeyString != "" {
		writeResponse(&w, true, "", cookie.Encode("AUTH", sessionKeyString))
		return
	}

	// Create session key
	sessionKey := uuid.NewV4()

	// Save Session Key
	err = db_users.PutSessionKey(username, sessionKey.String())
	if err != nil {
		writeResponse(&w, false, "Cannot create session", "")
		return
	}

	// Write Cookie
	// cookie.WriteCookie(&w, "AUTH", sessionKey.String())

	writeResponse(&w, true, "", cookie.Encode("AUTH", sessionKey.String()))
}

func writeResponse(w *http.ResponseWriter, stat bool, msg string, token string) {
	b, _ := json.Marshal(response{
		Status:  stat,
		Message: msg,
		Token:   token,
	})
	(*w).Write(b)
}
