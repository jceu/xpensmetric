package db_users

import (
	"app/go_pkg/db"
	"database/sql"
	"errors"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Users struct {
	ID             int
	Username       string
	Password       string
	Email          string
	Verified       int
	DateRegistered string
	SessionKey     sql.NullString
	SessionExpire  sql.NullString
}

func Add(username, password, email string) error {
	conn, err := db.DBConnect()
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.Exec("INSERT INTO `users` (`username`, `password`, `email`) VALUES (?, ?, ?)", username, password, email)
	if err != nil {
		return err
	}

	return nil
}

func CheckUsername(username string) (bool, error) {
	conn, err := db.DBConnect()
	if err != nil {
		return false, err
	}
	defer conn.Close()

	count := 0
	err = conn.QueryRow("SELECT COUNT(*) FROM `users` WHERE `username` = ?", username).Scan(&count)
	if err != nil {
		return false, err
	}

	if count > 0 {
		return false, nil
	}

	return true, nil
}

func CheckEmail(email string) (bool, error) {
	conn, err := db.DBConnect()
	if err != nil {
		return false, err
	}
	defer conn.Close()

	count := 0
	err = conn.QueryRow("SELECT COUNT(*) FROM `users` WHERE `email` = ?", email).Scan(&count)
	if err != nil {
		return false, err
	}

	if count > 0 {
		return false, nil
	}

	return true, nil
}

func HashPassword(password string) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

func VerifyPassword(password, hashed string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(password))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return errors.New("Wrong password supplied")
	}

	if err != nil {
		return err
	}

	return nil
}

func GetByUsername(username string) (Users, error) {
	conn, err := db.DBConnect()
	if err != nil {
		return Users{}, err
	}
	defer conn.Close()

	u := Users{}
	err = conn.QueryRow("SELECT * FROM `users` WHERE `username` = ?", username).Scan(&u.ID, &u.Username, &u.Password, &u.Email, &u.Verified, &u.DateRegistered, &u.SessionKey, &u.SessionExpire)
	if err != nil {
		return Users{}, err
	}

	return u, nil
}

func PutSessionKey(username string, sessionKey string) error {
	conn, err := db.DBConnect()
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.Exec("UPDATE `users` SET `session_key` = ?, `session_expire` = ? WHERE `username` = ?", sessionKey, time.Now().AddDate(0, 0, 7).Format("2006-01-02 15:04:05"), username)
	if err != nil {
		return err
	}

	return nil
}

func IsValidSessionKey(sessionKey string) bool {
	conn, err := db.DBConnect()
	if err != nil {
		return false
	}
	defer conn.Close()

	var expireDate string
	conn.QueryRow("SELECT `session_expire` FROM `users` WHERE `session_key` = ?", sessionKey).Scan(&expireDate)

	sevenDaysBefore := time.Now().AddDate(0, 0, -7).Unix()

	ed, _ := time.Parse("2006-01-02 15:04:05", expireDate)
	expireDateTimestamp := ed.Unix()

	if sevenDaysBefore > expireDateTimestamp {
		return false
	}

	return true
}

func GetSessionKey(username string) string {
	conn, err := db.DBConnect()
	if err != nil {
		return ""
	}
	defer conn.Close()

	var sessionKey string
	var sessionExpire string
	err = conn.QueryRow("SELECT `session_key`, `session_expire` FROM `users` WHERE `username` = ?", username).Scan(&sessionKey, &sessionExpire)
	if err != nil {
		return ""
	}

	t, err := time.Parse("2006-01-02 15:04:05", sessionExpire)
	if err != nil {
		return ""
	}

	if t.Unix() < time.Now().AddDate(0, 0, -7).Unix() {
		return ""
	}

	return sessionKey
}
