package records

import (
	"app/go_pkg/cookie"
	"app/go_pkg/db_moccurence"
	"app/go_pkg/db_users"
	"encoding/json"
	"errors"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

type Add struct{}

type Edit struct{}

type response struct {
	Status  bool
	Message string
	Token   string
}

type postData struct {
	Token             string
	Description       string
	Amount            string
	Date              string
	Occurence         string
	OccurenceType     string
	UntilDate         string
	ContinueForDay    string
	ContinueForWeekly string
	ContinueForMonth  string
	ContinueForYear   string
}

func (h *Add) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		writeResponse(&w, false, "Request method is invalid", "")
		return
	}

	err := r.ParseForm()
	if err != nil {
		writeResponse(&w, false, "Posted form cannot be parse", "")
		return
	}

	// Map posted value to struct
	pd := postData{
		Token:             r.Form.Get("token"),
		Description:       r.Form.Get("description"),
		Amount:            r.Form.Get("amount"),
		Date:              r.Form.Get("date"),
		Occurence:         r.Form.Get("occurence"),
		OccurenceType:     r.Form.Get("occurenceType"),
		UntilDate:         r.Form.Get("untilDate"),
		ContinueForDay:    r.Form.Get("continueForDay"),
		ContinueForWeekly: r.Form.Get("continueForWeekly"),
		ContinueForMonth:  r.Form.Get("continueForMonth"),
		ContinueForYear:   r.Form.Get("continueForYear"),
	}

	// Validate the posted data
	if err = Validate(pd); err != nil {
		writeResponse(&w, false, err.Error(), "")
		return
	}

	// TODO: Add the record to the database
	// TODO: Return the added record's id
}

func (h *Edit) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		writeResponse(&w, false, "Request method is invalid", "")
		return
	}

	// TODO: Check if token is valid
	// TODO: Validate the posted data
	// TODO: Check if the data is present in the database
}

func Validate(pd postData) error {
	// Check if token is valid
	if !db_users.IsValidSessionKey(cookie.Decode("AUTH", pd.Token)) {
		return errors.New("Unauthorize user detected. Try refreshing the page, your session might be expired.")
	}

	// Amount should be numeric
	if _, err := strconv.ParseFloat(pd.Amount, 64); err != nil {
		return errors.New("Invalid amount value.")
	}

	// Date must be a valid date (YYYY-m-d)
	if !isValidDateFormat(pd.Date) {
		return errors.New("Invalid date value.")
	}

	// Recurrence is present in master table
	occurenceIntVal, err := strconv.Atoi(pd.Occurence)
	if err != nil {
		return errors.New("Recurrence value is not valid.")
	}
	if !db_moccurence.OccurenceIDExists(occurenceIntVal) {
		return errors.New("Invalid recurrence value.")
	}

	// If Recurrence is "One Time", validation below is unnecessary
	if occurenceIntVal == db_moccurence.VAL_ONE_TIME {
		return nil
	}

	// OccurenceType is either "confor" OR "until"
	if !(pd.OccurenceType == "confor" || pd.OccurenceType == "until") {
		return errors.New("Invalid recurrence type.")
	}

	// If OccurenceType is "confor"
	if pd.OccurenceType == "confor" {
		// Days must be numeric (int)
		_, err := strconv.Atoi(pd.ContinueForDay)
		if err != nil {
			return errors.New("Day/s value is not valid.")
		}

		// Weeks must be numeric (int)
		_, err = strconv.Atoi(pd.ContinueForWeekly)
		if err != nil {
			return errors.New("Week/s value is not valid.")
		}

		// Months must be numeric (int)
		_, err = strconv.Atoi(pd.ContinueForMonth)
		if err != nil {
			return errors.New("Month/s value is not valid.")
		}

		// Years must be numeric (int)
		_, err = strconv.Atoi(pd.ContinueForYear)
		if err != nil {
			return errors.New("Year/s value is not valid.")
		}
	}

	// If OccurenceType is "until"
	if pd.OccurenceType == "until" {
		// UntilDate must be a valid date
		if !isValidDateFormat(pd.Date) {
			return errors.New("Invalid date value.")
		}

		// TODO: UntilDate must be equal or greater than Date
		t, err := time.Parse("2006-01-02", pd.Date)
		if err != nil {
			return errors.New("Date value cannot be parse.")
		}
		if !t.After(time.Now()) {
			return errors.New("Date value must be passed the current date.")
		}
	}

	return nil
}

func isValidDateFormat(date string) bool {
	rgx, err := regexp.Compile(`^\d{4}\-\d{2}\-\d{2}$`)
	if err != nil {
		return false
	}
	if !rgx.Match([]byte(date)) {
		return false
	}

	return true
}

func writeResponse(w *http.ResponseWriter, stat bool, msg string, token string) {
	b, _ := json.Marshal(response{
		Status:  stat,
		Message: msg,
		Token:   token,
	})
	(*w).Write(b)
}
