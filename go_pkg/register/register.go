package register

import (
	"app/go_pkg/cors"
	"app/go_pkg/db_users"
	"encoding/json"
	"net/http"

	"github.com/badoux/checkmail"
)

type Handle struct {
}

type response struct {
	Status  bool
	Message string
}

func (h *Handle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	cors.EnableAll(&w)

	if r.Method != http.MethodPost {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "GET method is not allowed.",
		})
		w.Write(b)
		return
	}

	err := r.ParseMultipartForm(10 << 20)
	if err != nil {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "Posted form cannot be parse.",
		})
		w.Write(b)
		return
	}

	username := r.PostForm.Get("username")
	password := r.PostForm.Get("password")
	email := r.PostForm.Get("email")

	ok, err := db_users.CheckUsername(username)
	if err != nil {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "Username verification failed.",
		})
		w.Write(b)
		return
	}

	if !ok {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "Username is already used.",
		})
		w.Write(b)
		return
	}

	ok, err = db_users.CheckEmail(email)
	if err != nil {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "Email verification failed.",
		})
		w.Write(b)
		return
	}

	if !ok {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "Email address is already used.",
		})
		w.Write(b)
		return
	}

	err = checkmail.ValidateFormat(email)
	if err != nil {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "Email address is not valid.",
		})
		w.Write(b)
		return
	}

	password, err = db_users.HashPassword(password)
	if err != nil {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "Password hashing failed.",
		})
		w.Write(b)
		return
	}

	err = db_users.Add(username, password, email)
	if err != nil {
		b, _ := json.Marshal(response{
			Status:  false,
			Message: "User registration failed.",
		})
		w.Write(b)
		return
	}

	b, _ := json.Marshal(response{
		Status:  true,
		Message: "",
	})
	w.Write(b)
}
