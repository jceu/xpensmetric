package db

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

const (
	DBUser = "root"
	DBPass = "root"
	DBHost = "127.0.0.1"
	DBPort = "3306"
	DBName = "xpens"
)

func DBConnect() (*sql.DB, error) {
	db, err := sql.Open("mysql", DBUser+":"+DBPass+"@tcp("+DBHost+":"+DBPort+")/"+DBName)
	if err != nil {
		return &sql.DB{}, err
	}

	return db, nil
}
