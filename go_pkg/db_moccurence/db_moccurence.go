package db_moccurence

import (
	"app/go_pkg/db"
	"encoding/json"
)

const (
	VAL_ONE_TIME int = 1
	VAL_DAILY    int = 2
	VAL_WEEKLY   int = 3
	VAL_MONTHLY  int = 4
	VAL_YEARLY   int = 5
)

type MOccurence struct {
	ID   int    `json:"id"`
	Text string `json:"text"`
}

func GetAll() []MOccurence {
	conn, err := db.DBConnect()
	if err != nil {
		return []MOccurence{}
	}
	defer conn.Close()

	var mo MOccurence
	var result []MOccurence

	rows, _ := conn.Query("SELECT * FROM `m_occurence`")
	for rows.Next() {
		rows.Scan(&mo.ID, &mo.Text)
		result = append(result, mo)
	}

	return result
}

func ConvertToJSON(data []MOccurence) (string, error) {
	b, err := json.Marshal(data)
	if err != nil {
		return "", err
	}

	return string(b), err
}

func OccurenceIDExists(id int) bool {
	conn, err := db.DBConnect()
	if err != nil {
		return false
	}
	defer conn.Close()

	count := 0
	err = conn.QueryRow("SELECT COUNT(*) FROM `m_occurence` WHERE `id` = ?", id).Scan(&count)
	if err != nil {
		return false
	}

	if count > 0 {
		return true
	}

	return false
}
