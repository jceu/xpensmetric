package public

import (
	"app/go_pkg/cookie"
	"app/go_pkg/cors"
	"app/go_pkg/db_moccurence"
	"app/go_pkg/db_users"
	"encoding/json"
	"net/http"
)

type response struct {
	Status  bool
	Message string
	Token   string
}

type OccurenceMaster struct {
}

func (h *OccurenceMaster) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	cors.EnableAll(&w)

	if r.Method != http.MethodPost {
		writeResponse(&w, false, "GET method is not allowed", "")
		return
	}

	err := r.ParseForm()
	if err != nil {
		writeResponse(&w, false, "Posted form cannot be parse", "")
		return
	}

	authCookie := r.Form.Get("token")

	// Check Session
	if !db_users.IsValidSessionKey(cookie.Decode("AUTH", authCookie)) {
		writeResponse(&w, false, "Session expired", "")
		return
	}

	jsonString, err := db_moccurence.ConvertToJSON(db_moccurence.GetAll())
	if err != nil {
		writeResponse(&w, false, "Can't fetch master data", "")
		return
	}

	writeResponse(&w, true, jsonString, authCookie)
}

func writeResponse(w *http.ResponseWriter, stat bool, msg string, token string) {
	b, _ := json.Marshal(response{
		Status:  stat,
		Message: msg,
		Token:   token,
	})
	(*w).Write(b)
}
